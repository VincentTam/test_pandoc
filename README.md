# Titre
## Section 1
Une liste numérotée.

1. premier élément
2. deuxième point

## Section 2
Une formule mathématique.

$$ \sum_{i=1}^n i = \frac{n(n+1)}{2} $$

Un diagramme mermaid

```mermaid
graph LR
A(sans redéfinition du point d'entrée) -- GitLab CI/CD --> B[échec]
A(avec redéfinition du point d'entrée) -- GitLab CI/CD --> C[réussi]
```

Un diagramme de classe UML

```{.mermaid format=svg}
classDiagram
  class MaEntité {
    + maCléPrimaire
    + monAttribut1
    + monAttribut2
  }
```
